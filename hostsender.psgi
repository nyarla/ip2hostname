#!/usr/bin/perl

use strict;
use warnings;

use LWP::UserAgent;
use Plack::Request;

package hostsender;

sub run {
    my ( $class ) = @_;

    my $app = $class->new;
       $app->setup_config;
       $app->setup_cache;
       $app->setup_agent;
       $app->setup_url;
       $app->setup_image;

    return $app->to_app;
}

sub new {
    bless {}, shift;
}

sub setup_config {
    my ( $self ) = @_;

    my $file    = env_value('config');
    my $config  = eval { require $file };

    die "failed to load configuration file: ${file}: ${@}" if ( $@ );
    die "configuration data is not HASH reference: ${config}" if ( ref $config ne 'HASH' );

    $self->{'config'} = $config;
}

sub setup_cache {
    my ( $self ) = @_;

    my %config = %{ $self->{'config'}->{'cache'} || {} };

    my $class   = delete $config{'class'} or die "Cache class is not specified.";
    my $args    = delete $config{'args'} || {};
    my $deref   = ( !! $config{'deref'} );

    my $module  = $class;
       $module  =~ s{::}{/}g;
       $module  = "${module}.pm";

    local $@;
    eval { require $module; };
    die "failed to load cache class: ${class}: ${@}" if ( $@ );

    my @args;
    if ( $deref ) {
        if ( ref $args eq 'ARRAY' ) {
            @args = @{ $args };
        }
        elsif ( ref $args eq 'HASH' ) {
            @args = %{ $args };
        }
        else {
            die "Cannot dereference: ${args}";
        }
    }
    else {
        push @args, $args;
    }

    my $instance = $class->new( @args );
    $self->{'cache'} = $instance;
}

sub setup_agent {
    my ( $self ) = @_;

    my %args    = %{ $self->{'config'}->{'agent'} || {} };
    my $ua      = LWP::UserAgent->new( %args );

    $self->{'agent'} = $ua;
}

sub setup_url {
    my ( $self ) = @_;
    my %config = %{ $self->{'config'}->{'send'} };

    my $url     = delete $config{'url'} or die "Send url is not specified.";
    my $param   = delete $config{'param'} || {};

    $self->{'url'}      = $url;
    $self->{'param'}    = $param;
}

sub setup_image {
    my ( $self ) = @_;

    my $source = q{K1TE&.#EA`0`!`(```/_______R'Y!`$*``$`+``````!``$```("3`$`.P``};
    my $image  = unpack('u*', $source);
    my $length = length($image);

    $self->{'image'} = {
        binary => $image,
        length => $length,
    };
}

sub to_app {
    my ( $self ) = @_;

    my $cache   = $self->{'cache'};
    my $agent   = $self->{'agent'};
    my $url     = $self->{'url'};
    my $param   = $self->{'param'};
    my $image   = $self->{'image'}->{'binary'};
    my $length  = $self->{'image'}->{'length'};

    return sub {
        my $env = shift;
        my $req = Plack::Request->new($env);

        my $ip      = $req->address;
        my $host    = $req->hostname;
        my $old     = $cache->get($ip) || q{};

        if ( $host ne $old ) {
            $cache->set( $ip => $host );

            my %param               = %{ $param };
               $param{'ip'}         = $ip;
               $param{'hostname'}   = $host;

            $agent->post( $url, { %param } );
        }

        return [
            200,
            [
                'Content-Type'      => 'image/gif',
                'Content-Length'    => $length,
            ],
            [ $image ],
        ];
    };
}

sub env_value {
    my $prefix  = uc __PACKAGE__;
    my $key     = uc $_[0];
    my $env     = "${prefix}_${key}";

    if ( exists $ENV{$env} ) {
        return $ENV{$env};
    }

    return;
}

package main;

hostsender->run if ( ! hostsender::env_value('libmode') );

1;

=head1 NAME

hostsender - send IP address and hostname.

=head1 SYNPOSIS

    $ pwd
    /home/nyarla/dev
    $ git clone git://github.com/nyarla/ip2hostname.git
    $ mkdir hostsender
    $ touch hostsender/config.pl
    $ vim hostsender/config.pl
    $ export HOSTSERNDER_CONFIG=/home/nyarla/dev/hostsender/config.pl
    $ plackup --app ip2hostname/hostsender.psgi

=head1 DESCRIPTION

This psgi script sends IP address and hostname of
the person who has accessed this script to specified URL.

This script sends information using the POST method.

Only when the IP address and hostname who accessed is different from last access,
this script sends an IP address and hostname.

=head1 CONFIG

    #!perl
    
    use strict;
    use warnings;
    
    return {
    
        # cache setting
        cache   => {
            # cache class name
            class   => 'Cache::FileCache',
            # arguments
            args    => {
                cache_root  => '/path/to/cache/root',
                cache_depth => 1,
            },
            # dereference
            deref   => 0,
        },
    
        # LWP::UserAgetn arguments
        agent   => {
            agent   => 'hostsender/0.1',
            timeout => 1,
        },
    
        # send url and parameter settings
        send    => {
            url     => 'http://example.com',
            param   => {
                foo => 'bar',
            },
        },
    };

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This script under public domain.

=cut
