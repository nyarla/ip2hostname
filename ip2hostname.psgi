#!/usr/bin/perl

use strict;
use warnings;

use Socket qw( AF_INET inet_aton );
use Regexp::Common qw( net );

my $header = [
    'Content-Type' => 'text/plain',
];

return sub {
    my $env = shift;
    my $path_info = $env->{'PATH_INFO'};

    if ( $path_info =~ m{^/($RE{net}{IPv4})} ) {
        my $ip  = $1;
        my $host;

        local $@;
        eval {
            $host = gethostbyaddr( inet_aton( $ip ), AF_INET );
        };

        if ( ! $host || $@ ) {
            return [ 404, $header, ['hostname is not found'] ];
        }

        if ( $host ) {
            return [ 200, $header, [ $host ] ];
        }
    }
    else {
        return [ 500, $header, ['invalid argument. usage: /{IPv4}'] ];
    }
};

__END__

=head1 NAME

ip2hostname.psgi - Converter of IP address to hostname.

=head1 SYNPOSIS

    $ plackup --app ip2hostname.psgi

and access to B<http://{yourdomain}/{IPv4}>.

=head1 DESCRIPTION

This psgi script is converter of IP address to hostname.

=head1 REQUIREMENT MODULES

=over

=item L<Socket>

=item L<Regexp::Common>

=back

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This script is under public domain.

=cut
