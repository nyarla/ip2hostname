#!perl

use strict;
use warnings;

use t::Util qw( $ip2hostname );
use Test::More tests => 9;

my $handler = require $ip2hostname->stringify;

# invalid
my $res = $handler->( { PATH_INFO => '/fooooooooooooo!' } );

is(         $res->[0], 500 );
is_deeply(  $res->[1], [ 'Content-Type' => 'text/plain' ] );
is_deeply(  $res->[2], ['invalid argument. usage: /{IPv4}'] );

$res = $handler->({ PATH_INFO => '' });
is(         $res->[0], 500 );
is_deeply(  $res->[1], [ 'Content-Type' => 'text/plain' ] );
is_deeply(  $res->[2], ['invalid argument. usage: /{IPv4}'] );

# get hostnmae
$res = $handler->({ PATH_INFO => '/8.8.8.8' });

is(         $res->[0], 200 );
is_deeply(  $res->[1], [ 'Content-Type' => 'text/plain' ]);
is_deeply(  $res->[2], ['google-public-dns-a.google.com']);
