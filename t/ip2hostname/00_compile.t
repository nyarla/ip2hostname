#!perl

use strict;
use warnings;

use Test::More tests => 1;
use t::Util qw( $ip2hostname );

BEGIN { require_ok( $ip2hostname->stringify ) }
