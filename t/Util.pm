package t::Util;

use strict;
use warnings;

use base qw( Exporter );

use Path::Class;
use FindBin ();

our ( $basedir, $ip2hostname, $hostsender, $examples );

our @EXPORT_OK = qw(
    $basedur $examples
    $ip2hostname $hostsender
    require_hostsender
);

{
    my @path = dir($FindBin::Bin)->dir_list;
    while ( my $dir = pop @path ) {
        if ( $dir eq 't' ) {
            $basedir        = dir(@path);
            $examples       = $basedir->subdir('t/examples');
            $ip2hostname    = $basedir->file('ip2hostname.psgi');
            $hostsender     = $basedir->file('hostsender.psgi');
        }
    }
}

sub require_hostsender {
    local $ENV{'HOSTSENDER_LIBMODE'} = 1;
    require $hostsender->stringify;
}

1;
