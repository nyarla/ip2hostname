#!perl

use strict;
use warnings;

use t::Util qw( require_hostsender );
use Test::More tests => 1;

require_hostsender;

isa_ok( hostsender->new, 'hostsender' );
