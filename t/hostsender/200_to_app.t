#!perl

use strict;
use warnings;

use t::Util qw( require_hostsender );
use Test::Requires qw(
    Test::TCP
    Plack::Test
    Plack::Loader
    Plack::Request
    HTTP::Request
);
use Test::More;

require_hostsender;

my $app = hostsender->new;
   $app->{'config'} = {
        cache   => {
            class   => 'Cache::MemoryCache',
            args    => {},
        },
        agent   => {
            agent   => 'hostsender/0.1',
        },
        send    => {
            url     => 'http://localhost/',
            param   => {
                foo => 'bar',
            },
        },
   };
   $app->setup_cache;
   $app->setup_agent;
   $app->setup_url;
   $app->setup_image;

my $image   = unpack('u*', q{K1TE&.#EA`0`!`(```/_______R'Y!`$*``$`+``````!``$```("3`$`.P``});
my $length  = length($image);

test_tcp(
    client => sub {
        my ( $port ) = @_;
        test_psgi(
            $app->to_app,
            sub {
                my ( $callback ) = @_;
                my $req = HTTP::Request->new( GET => "http://localhost:${port}/" );
                my $res = $callback->($req);

                ok( $res->is_success );
                is( $res->code, 200 );
                is( $res->headers->content_type, 'image/gif' );
                is( $res->headers->content_length, $length );
                is( $res->content, $image );
            },
        );

    },
    server => sub {
        my ( $port ) = @_;
        my $server = Plack::Loader->auto( port => $port, host => '127.0.0.1' );
           $server->run(sub {
                my $req = Plack::Request->new(shift);

                is( $req->method, 'POST' );
                is( $req->params('ip'),         '127.0.0.1' );
                is( $req->params('hostname'),   'localhost' );
                is( $req->params('foo'),        'bar' );

                return [ 200, [ 'Conetnt-Type' => 'text/plain' ], [ q{} ] ];
           })
    },
);

done_testing;
