#!perl

use strict;
use warnings;

use t::Util qw( require_hostsender );
use Test::Requires qw( Cache::Memory Cache::MemoryCache );
use Test::More tests => 4;

require_hostsender;

my $app = hostsender->new;
   $app->{'config'} = {
        cache => {
            class   => 'Cache::Memory',
            args    => { default_expires => '60 sec' },
            deref   => 1,
        }
   };

$app->setup_cache;

isa_ok($app->{'cache'}, 'Cache::Memory');
like( $app->{'cache'}->default_expires, qr{^\d+$} );

$app->{'config'} = {
    cache => {
        class   => 'Cache::MemoryCache',
        args    => { default_expires_in => 60 },
    }
};

$app->setup_cache;

isa_ok($app->{'cache'}, 'Cache::MemoryCache');
is( $app->{'cache'}->get_default_expires_in, 60 );
