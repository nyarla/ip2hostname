#!perl

use strict;
use warnings;

use t::Util qw( require_hostsender );
use Test::More tests => 2;

require_hostsender;

my $app = hostsender->new;
   $app->{'config'} = {
        send => {
            url     => 'http://example.com/',
            param   => {
                foo => 'bar',
            },
        },
   };

$app->setup_url;

is( $app->{'url'}, 'http://example.com/' );
is_deeply( $app->{'param'}, { foo => 'bar' } );
