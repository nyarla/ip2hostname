#!perl

use strict;
use warnings;

use t::Util qw( require_hostsender $examples );
use Test::More tests => 1;

require_hostsender;

local $ENV{'HOSTSENDER_CONFIG'} = $examples->file('hostsender/loadtest.pl');

my $app = hostsender->new;

$app->setup_config;

is_deeply(
    $app->{'config'},
    {
        foo => 'bar',
    }
);
