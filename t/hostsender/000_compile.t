#!perl

use strict;
use warnings;

use t::Util qw( $hostsender );
use Test::More tests => 1;

BEGIN {
    local $ENV{'HOSTSENDER_LIBMODE'} = 1;
    require_ok( $hostsender->stringify );
}
