#!perl

use strict;
use warnings;

use t::Util qw( require_hostsender );
use Test::More tests => 1;

require_hostsender;

my $app = hostsender->new;
   $app->setup_image;

my $image   = unpack('u*', q{K1TE&.#EA`0`!`(```/_______R'Y!`$*``$`+``````!``$```("3`$`.P``});
my $length  = length($image);

is_deeply(
    $app->{'image'},
    {
        binary  => $image,
        length  => $length,
    },
);
