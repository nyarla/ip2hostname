#!perl

use strict;
use warnings;

use t::Util qw( require_hostsender );
use Test::More tests => 2;

require_hostsender;

my $app = hostsender->new;
   $app->{'config'} = {
        agent => {
            agent => 'hostsender/0.1',
        },
   };
   
$app->setup_agent;

isa_ok( $app->{'agent'}, 'LWP::UserAgent' );

is( $app->{'agent'}->agent, 'hostsender/0.1' );
