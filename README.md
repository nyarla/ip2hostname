# NAME

ip2hostname.psgi - Converter of IP address to hostname.

# SYNPOSIS

    $ plackup --app ip2hostname.psgi

and access to `http://{yourdomain}/{IPv4}`.

# DESCRIPTION

This psgi script is converter of IP address to hostname.

# REQUIREMENT MODULES

* Socket
* Regexp::Common

# AUTHOR

Naoki Okamura (Nyarla) <nyarla[ at ]thtep.net>

# LICENSE

This script is under public domain.

